import React from "react";
import SignUpReduxForm, {SignUpValuesType} from "../../../../ud-forms/ui/signUp_form/sign_up_form";
import {RootStateType} from "../../../../store/reducer";
import {requestPostSignUp} from "../../../store/signIn_signUp/reducer";
import {connect, ConnectedProps} from "react-redux";

const mapState = (state: RootStateType) => ({
    signInToken: state.session.signIn.token
});

const mapDispatch = {
    requestPostSignUp
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

type SignUpProps = PropsFromRedux & {}

const SignUp = (props: SignUpProps) => {

    const onSubmit = (formData: SignUpValuesType) => {
        props.requestPostSignUp(formData)
    }

    return <div className="container-fluid inherit-height d-flex align-items-center">
        <div className="container">
            <div className="row align-items-center justify-content-center">
                <p className="semibold-text fs18">Dashboard</p>
            </div>
            <div className="row align-items-center justify-content-center">
                <div className="col-4 card p-3">
                    <SignUpReduxForm onSubmit={onSubmit}/>
                    <div className="row align-items-center justify-content-center mt-4">
                        <p className="regular-text disabled-text fs12">Not registered?</p>
                        <a href="/users/sign-in"><p className="semibold-text fs15 blue m-1">Sign in</p></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
};

export default connector(SignUp);