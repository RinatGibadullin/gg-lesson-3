import React from "react";
import SignInReduxForm, {SignInValuesType} from "../../../../ud-forms/ui/signIn_form/sign_in_form";
import {RootStateType} from "../../../../store/reducer";
import {connect, ConnectedProps} from "react-redux";
import {requestPostSignIn} from "../../../store/signIn_signUp/reducer";
import { Redirect } from "react-router-dom";

const mapState = (state: RootStateType) => ({
    signInToken: state.session.signIn.token,
    isLoggedIn: state.session.signIn.isLoggedIn
});

const mapDispatch = {
    requestPostSignIn
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

type SignInProps = PropsFromRedux & {}

const SignIn = (props: SignInProps) => {
    const onSubmit = (formData: SignInValuesType) => {
        props.requestPostSignIn(formData)
    }

    if (props.isLoggedIn) {
        return <Redirect to={"/dashboard"}/>
    }


    return <div className="container-fluid inherit-height d-flex align-items-center">
        <div className="container">
            <div className="row align-items-center justify-content-center">
                <p className="semibold-text fs18">Dashboard</p>
            </div>
            <div className="row align-items-center justify-content-center">
                <div className="col-4 card p-3">
                    <SignInReduxForm onSubmit={onSubmit}/>
                    <div className="row align-items-center justify-content-center mt-4">
                        <p className="regular-text disabled-text fs12">Not registered?</p>
                        <a href="/users/sign-up"><p className="semibold-text fs15 blue m-1">Sign up</p></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
};

export default connector(SignIn);