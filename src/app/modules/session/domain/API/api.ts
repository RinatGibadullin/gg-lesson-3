import instance from "../../../core/API";
import {AxiosResponse} from "axios";
import {NewContactValuesType} from "../../../ud-forms/ui/new-contact-form";
import {SignInValuesType} from "../../../ud-forms/ui/signIn_form/sign_in_form";
import {SignUpValuesType} from "../../../ud-forms/ui/signUp_form/sign_up_form";

export const sessionAPI = {
    sign_in(values: SignInValuesType) {
        return instance.post(`users/sign-in`, values)
    },

    sign_up(values: SignUpValuesType) {
        return instance.post(`users/sign-up`, values)
    }
}