import {Dispatch} from "redux";
import {RootStateType} from "../../../store/reducer";
import {SignInValuesType} from "../../../ud-forms/ui/signIn_form/sign_in_form";
import {sessionAPI} from "../../domain/API/api";
import {SET_IS_LOGGED_IN, SET_TOKEN, setIsLoggedIn, SetSignInActionsType, setSignInToken} from "./actions";
import {SignUpValuesType} from "../../../ud-forms/ui/signUp_form/sign_up_form";
import {AxiosResponse} from "axios";
import {FormAction, stopSubmit} from "redux-form";

const SignInInitialState = {
    token: "" as string,
    isLoggedIn: false as boolean
}

type SignInInitialStateType = typeof SignInInitialState

export const signInReducer = (state = SignInInitialState, action: SetSignInActionsType): SignInInitialStateType => {
    switch (action.type) {
        case SET_TOKEN:
            return {
                ...state,
                token: action.token
            }
        case SET_IS_LOGGED_IN:
            return {
                ...state,
                isLoggedIn: action.isLoggedIn
            }
        default:
            return state
    }
};

export const requestPostSignIn = (values: SignInValuesType) => {
    return async (dispatch: Dispatch<SetSignInActionsType | FormAction>, getState: RootStateType) => {
        let response = await sessionAPI.sign_in(values)
        if (response.status === 200) {
            dispatch(setSignInToken(response.data.token))
            dispatch(setIsLoggedIn(true))
            localStorage.setItem("auth_token", response.data.token)
        } else {
            dispatch(stopSubmit("sign_in", {_error: "Invalid data"}))
        }
    }
}

export const requestPostSignUp = (values: SignUpValuesType) => {
    return async (dispatch: Dispatch<SetSignInActionsType>, getState: RootStateType) => {
        let response = await sessionAPI.sign_up(values)
        if (response.status === 200) {
            dispatch(setSignInToken(response.data.token))
            dispatch(setIsLoggedIn(true))
            localStorage.setItem("auth_token", response.data.token)
        }
    }
}