export const SET_TOKEN = 'SET_TOKEN';
export const SET_IS_LOGGED_IN = 'SET_IS_LOGGED_IN';

export type SetSignInTokenActionType = {
    type: typeof SET_TOKEN
    token: string
}

export type SetIsLoggedInActionType = {
    type: typeof SET_IS_LOGGED_IN
    isLoggedIn: boolean
}

export const setSignInToken = (token: string): SetSignInTokenActionType => ({type: SET_TOKEN, token});
export const setIsLoggedIn = (isLoggedIn: boolean): SetIsLoggedInActionType => ({type: SET_IS_LOGGED_IN, isLoggedIn});

export type SetSignInActionsType = SetIsLoggedInActionType | SetSignInTokenActionType