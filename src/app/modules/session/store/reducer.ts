import {combineReducers} from "redux";
import {signInReducer} from "./signIn_signUp/reducer";

export const sessionRootReducer = combineReducers({
    signIn: signInReducer
});

export type SessionRootStateType = ReturnType<typeof sessionRootReducer>