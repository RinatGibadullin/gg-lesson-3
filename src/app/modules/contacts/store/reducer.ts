import {combineReducers} from "redux";
import {contactsReducer} from "./contacts-list/reducer";
import {newContactReducer} from "./new-contact/reducer";
import {contactReducer} from "./edit-contact/reducer";
import {rolesReducer} from "./roles/reducer";

export const contactsRootReducer = combineReducers({
    contactsList: contactsReducer,
    newContact: newContactReducer,
    contact: contactReducer,
    rolesReducer: rolesReducer
});

export type ContactsRootStateType = ReturnType<typeof contactsRootReducer>