import {Dispatch} from "redux";
import {RootStateType} from "../../../store/reducer";
import {contactsAPI} from "../../domain/API/contactsAPI";
import {SET_ROLES, setRoles, setRolesActionType} from "./actions";

const rolesInitialState = {
    roles: [] as string[],
}

type RolesInitialStateType = typeof rolesInitialState

export const rolesReducer = (state = rolesInitialState, action: setRolesActionType): RolesInitialStateType => {
    switch (action.type) {
        case SET_ROLES:
            return {
                ...state,
                roles: action.roles
            }

        default:
            return state
    }
};

export const requestRoles = () => {
    return async (dispatch: Dispatch<setRolesActionType>, getState: RootStateType) => {
        let data = await contactsAPI.get_roles()
        dispatch(setRoles(data))
    }
}