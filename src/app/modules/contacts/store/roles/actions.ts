export const SET_ROLES = 'SET_ROLES';

export type setRolesActionType = {
    type: typeof SET_ROLES
    roles: string[]
}

export const setRoles = (roles: string[]): setRolesActionType => ({type: SET_ROLES, roles});