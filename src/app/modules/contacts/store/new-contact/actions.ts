import {NewContactValuesType} from "../../../ud-forms/ui/new-contact-form";

export const SET_NEW_CONTACT = 'SET_NEW_CONTACT';

export type SetNewContactActionType = {
    type: typeof SET_NEW_CONTACT
    contact: NewContactValuesType
}

export const setNewContact = (contact: NewContactValuesType): SetNewContactActionType => ({type: SET_NEW_CONTACT, contact});