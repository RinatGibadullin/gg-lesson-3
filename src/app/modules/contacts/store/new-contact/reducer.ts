import {Dispatch} from "redux";
import {RootStateType} from "../../../store/reducer";
import {contactsAPI} from "../../domain/API/contactsAPI";
import {SET_NEW_CONTACT, setNewContact, SetNewContactActionType} from "./actions";
import {NewContactValuesType} from "../../../ud-forms/ui/new-contact-form";

const newContactInitialState = {
    contact: {} as NewContactValuesType
}

type NewContactInitialStateType = typeof newContactInitialState

export const newContactReducer = (state = newContactInitialState, action: SetNewContactActionType): NewContactInitialStateType => {
    switch (action.type) {
        case SET_NEW_CONTACT:
            return {
                ...state,
                contact: action.contact
            }
        default:
            return state
    }
};

export const requestPostNewContact = (contact: NewContactValuesType) => {
    return async (dispatch: Dispatch<SetNewContactActionType>, getState: RootStateType) => {
        console.log(contact)
        let data = await contactsAPI.post_new_contact(contact)
        dispatch(setNewContact(data))
    }
}