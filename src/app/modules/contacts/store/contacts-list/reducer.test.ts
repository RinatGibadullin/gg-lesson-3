import {contactsInitialState, contactsReducer} from "./reducer";
import {ContactType, setContacts} from "./actions";

it(`length of contacts array should be incremented`, () => {
    const contacts: ContactType[] = [
        {
            id: 1,
            name: "Rinat",
            email: "Rinat",
            company: "google",
            role: "CEO",
            forecast: "50%",
            recentActivity: "5 minute ago"
        },
        {
            id: 2,
            name: "Rinat",
            email: "Rinat",
            company: "google",
            role: "CEO",
            forecast: "50%",
            recentActivity: "5 minute ago"
        }
    ]

    const action = setContacts(contacts);
    const newState = contactsReducer(contactsInitialState, action)
    expect(newState.contacts.length).toBe(2)
})