export const SET_CONTACTS = 'SET_CONTACTS';
export const SET_CONTACTS_PAGINATION = 'SET_CONTACTS_PAGINATION';


export type ContactType = {
    id: number,
    name: string,
    email: string,
    company: string,
    role: string,
    forecast: string,
    recentActivity: string
}

export type ContactsPagination = {
    offset: number,
    limit: number
}

export type setContactsActionType = {
    type: typeof SET_CONTACTS
    contacts: Array<ContactType>
}

export type setContactsPaginationActionType = {
    type: typeof SET_CONTACTS_PAGINATION
    pagination: ContactsPagination
}

export const setContacts = (contacts: Array<ContactType>): setContactsActionType => ({type: SET_CONTACTS, contacts});

export const setContactsPagination = (pagination: ContactsPagination): setContactsPaginationActionType => (
    {type: SET_CONTACTS_PAGINATION, pagination}
);

export type ContactsActionsType = setContactsActionType | setContactsPaginationActionType