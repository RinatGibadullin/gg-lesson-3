import {createSelector} from "reselect";
import {RootStateType} from "../../../store/reducer";


const getContacts = (state: RootStateType) => state.contacts.contactsList.contacts;
const getPagination = (state: RootStateType) => state.contacts.contactsList.pagination

export const getContactsSelector = createSelector(getContacts, (contacts) => {
    return contacts
})

export const getPaginationSelector = createSelector(getPagination, (pagination) => {
    return pagination
})