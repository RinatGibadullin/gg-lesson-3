import {
    ContactsActionsType,
    ContactsPagination,
    ContactType,
    SET_CONTACTS,
    SET_CONTACTS_PAGINATION,
    setContacts
} from "./actions";
import {Dispatch} from "redux";
import {RootStateType} from "../../../store/reducer";
import {contactsAPI} from "../../domain/API/contactsAPI";

export const contactsInitialState = {
    contacts: [] as ContactType[],
    pagination: {
        offset: 0, limit: 10
    } as ContactsPagination
}

type ContactsInitialStateType = typeof contactsInitialState

export const contactsReducer = (state = contactsInitialState, action: ContactsActionsType): ContactsInitialStateType => {
    switch (action.type) {
        case SET_CONTACTS:
            return {
                ...state,
                contacts: [...state.contacts, ...action.contacts]
            }
        case SET_CONTACTS_PAGINATION:
            return {
                ...state,
                pagination: {
                    ...state.pagination,
                    offset: (state.pagination.offset + state.pagination.limit)
                }
            }
        default:
            return state
    }
};

export const requestContacts = (offset: number, limit: number) => {
    return async (dispatch: Dispatch<ContactsActionsType>, getState: RootStateType) => {
        let data = await contactsAPI.get_contacts(offset, limit)
        dispatch(setContacts(data))
    }
}