import {Dispatch} from "redux";
import {contactsAPI} from "../../domain/API/contactsAPI";
import {ContactType} from "../contacts-list/actions";
import {SET_CONTACT, setContact, SetContactByIdActionType} from "./actions";

const contactByIdInitialState = {
    contact: {} as ContactType
}

type ContactByIdInitialStateType = typeof contactByIdInitialState

export const contactReducer = (state = contactByIdInitialState, action: SetContactByIdActionType): ContactByIdInitialStateType => {
    switch (action.type) {
        case SET_CONTACT:
            return {
                contact: action.contact
            }
        default:
            return state
    }
};

export const requestGetContactById = (id: number) => {
    return async (dispatch: Dispatch<SetContactByIdActionType>) => {
        console.log(id)
        let data = await contactsAPI.get_contact_by_id(id)
        // debugger
        dispatch(setContact(data))
    }
}

export const requestUpdateContact = (contact: ContactType) => {
    return async (dispatch: Dispatch<SetContactByIdActionType>) => {
        console.log(contact)
        let data = await contactsAPI.update_contact(contact)
    }
}