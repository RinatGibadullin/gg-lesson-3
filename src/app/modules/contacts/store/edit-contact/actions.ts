import {ContactType} from "../contacts-list/actions";

export const SET_CONTACT = 'SET_CONTACT';

export type SetContactByIdActionType = {
    type: typeof SET_CONTACT
    contact: ContactType
}

export const setContact = (contact: ContactType): SetContactByIdActionType => ({type: SET_CONTACT, contact});