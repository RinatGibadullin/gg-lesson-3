import React from 'react';
import UDButton from "../../../../ud-ui/button/button";
import ContactsList from "../../components/contacts-list";

type props = {};

const Contacts = (props: props) => {
    return <div className="container-fluid inherit-height mt-2">
        <div className="row align-items-end justify-content-between m-0 py-4">
            <a href="#" className="medium-text disabled-text fs15">Company: All</a>
            <UDButton label="Add contact" type="lg" href="/contacts/new"/>
        </div>
        <div className="row m-0">
            <ContactsList/>
        </div>
    </div>;
};

export default Contacts;
