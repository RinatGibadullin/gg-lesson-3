import React, {useEffect} from 'react';
import {useParams} from "react-router-dom";
import UDButton from "../../../../ud-ui/button/button";
import {reduxForm} from "redux-form";
import {NewContactForm, NewContactValuesType} from "../../../../ud-forms/ui/new-contact-form";
import {RootStateType} from "../../../../store/reducer";
import {connect, ConnectedProps} from "react-redux";
import {requestGetContactById, requestUpdateContact} from "../../../store/edit-contact/reducer";
import {SET_CONTACT} from "../../../store/edit-contact/actions";
import {ContactType} from "../../../store/contacts-list/actions";
import {Roles} from "../../components/new-contact-card";
import {requestRoles} from "../../../store/roles/reducer";

const mapState = (state: RootStateType) => ({
    contact: state.contacts.contact.contact,
    roles: state.contacts.rolesReducer.roles
});

const mapDispatch = {
    requestGetContactById,
    requestUpdateContact,
    requestRoles
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

type EditContactProps = PropsFromRedux & {}

const EditContact = (props: EditContactProps) => {
    let {id} = useParams();

    useEffect(() => {
        props.requestGetContactById(id)
        props.requestRoles()
    },[])

    // const receivedContactById = {id: id, name: "Rinat", email: "Rinatos@ru", company:"google", role: "SEO"};
    // const {company, role, email, name} = receivedContactById;
    // const {name, email, company, role} = props.contact
    const EditContactReduxForm = reduxForm<ContactType, Roles>({
        form: 'edit_contact',
        initialValues: props.contact
    } )(NewContactForm);

    const onSubmit = (formData: ContactType) => {
        formData.id = props.contact.id
        props.requestUpdateContact(formData)
        console.log(formData)
    }

    return <div className="container-fluid mt-2 px-5">
        <div className="row align-items-center justify-content-between m-0 py-4">
            <p className="semibold-text fs18">Edit contact</p>
            <UDButton label="Add contact" type="lg" href="/contacts/new"/>
        </div>
        <div className="row col-lg-8 col-md-12 card">
            <EditContactReduxForm roles={props.roles} onSubmit={onSubmit}/>
        </div>
    </div>
};

export default connector(EditContact);