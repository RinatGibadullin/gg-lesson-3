import React from 'react';
import UDButton from "../../../../ud-ui/button/button";
import NewContactCard from "../../components/new-contact-card";

const NewContact = () => {
    return <div className="container-fluid mt-2 px-5">
        <div className="row align-items-center justify-content-between m-0 py-4">
            <p className="semibold-text fs18">New contact</p>
            <UDButton label="Add contact" type="lg" href="/contacts/new"/>
        </div>
        <div className="row col-lg-8 col-md-12">
            <NewContactCard/>
        </div>
    </div>
};

export default NewContact;