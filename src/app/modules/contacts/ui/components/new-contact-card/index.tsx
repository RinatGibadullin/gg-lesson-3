import React, {useEffect} from 'react';
import './styles.scss';
import NewContactReduxForm, {NewContactValuesType} from "../../../../ud-forms/ui/new-contact-form";
import {requestPostNewContact} from "../../../store/new-contact/reducer";
import {RootStateType} from "../../../../store/reducer";
import {connect, ConnectedProps} from "react-redux";
import {requestRoles} from "../../../store/roles/reducer";

export type Roles = {
    roles: string[]
}

const mapStateToProps = (state: RootStateType) => ({
    newContactState: state.contacts.newContact,
    roles: state.contacts.rolesReducer.roles
});

const mapDispatchToProps = {
    requestPostNewContact,
    requestRoles
};

const connector = connect(mapStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>


const NewContactCard = (props: PropsFromRedux) => {

    useEffect(() => {
        props.requestRoles()
    },[])

    const onSubmit = (formData: NewContactValuesType) => {
        props.requestPostNewContact(formData)
    }
    return <div className="container-fluid new-contact-card">
        <NewContactReduxForm roles={props.roles} onSubmit={onSubmit}/>
    </div>
};

export default connector(NewContactCard);
