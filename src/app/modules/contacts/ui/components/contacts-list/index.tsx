import React, {useEffect} from 'react';
import './styles.scss';
import ContactItem from "../contacts-item";
import UDCheckbox from "../../../../ud-ui/checkbox/checkbox";
import {RootStateType} from "../../../../store/reducer";
import {connect, ConnectedProps} from "react-redux";
import {requestContacts} from "../../../store/contacts-list/reducer";
import {SET_CONTACTS_PAGINATION} from "../../../store/contacts-list/actions";
import Loader from "react-loader";
import {getContactsSelector, getPaginationSelector} from "../../../store/contacts-list/selectors";

type contactsProps = {};

const mapState = (state: RootStateType) => ({
    contacts: getContactsSelector(state),
    pagination: getPaginationSelector(state)
});

const mapDispatch = {
    requestContacts,
    setContactsPagination: () => ({type: SET_CONTACTS_PAGINATION}),
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

type contactsListProps = PropsFromRedux & {}

const ContactsList = (props: contactsListProps) => {

    useEffect(() => {
        props.requestContacts(props.pagination.offset, props.pagination.limit)
    }, [props.pagination.offset]);

    return <div className="container-fluid m-0 p-0">
        <div className="container-fluid contacts-header-card">
            <div className="row align-items-center justify-content-between p-3">
                <div>
                    <UDCheckbox flag={false}/>
                </div>
                <div className="col col-3">
                    <a href="/" className="medium-text disabled-text fs15">Name</a>
                </div>
                <div className="col col-3">
                    <p className="medium-text disabled-text fs15">Email</p>
                </div>
                <div className="col col-1">
                    <p className="medium-text disabled-text fs15">Company</p>
                </div>
                <div className="col col-1">
                    <p className="medium-text disabled-text fs15">Role</p>
                </div>
                <div className="col col-1">
                    <p className="medium-text disabled-text fs15">Forecast</p>
                </div>
                <div className="col col-2">
                    <p className="medium-text disabled-text fs15">Recent activity</p>
                </div>
            </div>
        </div>
        {props.contacts.length === 0 ?
            (<Loader
                loaded={false}
                lines={13}
                length={20}
                width={10}
                radius={30}
                corners={1}
                rotate={0}
                direction={1}
                color="#000"
                speed={1}
                trail={60}
                shadow={false}
                hwaccel={false}
                className="spinner"
                zIndex={2e9}
                top="50%"
                left="50%"
                scale={1.0}
                loadedClassName="loadedContent"
            />)
            : (props.contacts.map(item =>
                <ContactItem
                    key={item.id}
                    checkboxValue={true} name={item.name}
                    email={item.email} company={item.company}
                    role={item.role} forecast={item.forecast} recentActivity={item.recentActivity}/>))
        }
        <a onClick={props.setContactsPagination}><p className="semibold-text fs15 blue row justify-content-center">Show
            more</p></a>
    </div>
};

export default connector(ContactsList);
