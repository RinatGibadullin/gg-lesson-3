import React from 'react';
import './styles.scss';
import UDCheckbox from "../../../../ud-ui/checkbox/checkbox";


export type ContactProps = {
    checkboxValue: boolean,
    name: string,
    email: string,
    company: string,
    role: string,
    forecast: string,
    recentActivity: string
};


const ContactItem = (props: ContactProps) => {
    return <div className="container-fluid contacts-item-card">
        <div className="row align-items-center justify-content-between p-3">
            <div>
                <UDCheckbox flag={props.checkboxValue}/>
            </div>
            <div className="col col-3 row align-items-center justify-content-start">
                <div className="contacts-item-name-avatar mr-3"/>
                <a href="/" className="semibold-text fs15">{props.name}</a>
            </div>
            <div className="col col-3">
                <p className="medium-text disabled-text fs15">{props.email}</p>
            </div>
            <div className="col col-1">
                <p className="medium-text disabled-text fs15">{props.company}</p>
            </div>
            <div className="col col-1">
                <p className="medium-text disabled-text fs15">{props.role}</p>
            </div>
            <div className="col col-1">
                <p className="medium-text disabled-text fs15">{props.forecast}</p>
            </div>
            <div className="col col-2">
                <p className="medium-text disabled-text fs15">{props.recentActivity}</p>
            </div>
        </div>
    </div>;
};

export default ContactItem;
