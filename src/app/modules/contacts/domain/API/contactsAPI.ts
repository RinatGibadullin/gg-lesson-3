import {ContactType} from "../../store/contacts-list/actions";
import {AxiosResponse} from "axios";
import instance from "../../../core/API";
import {NewContactValuesType} from "../../../ud-forms/ui/new-contact-form";

type GetContactsListType = {
    items: Array<ContactType>
}

export const contactsAPI = {
    get_contacts(offset: number, limit: number){
        return instance.get(`contacts?offset=${offset}&limit=${limit}`).then((response: AxiosResponse<GetContactsListType>) => response.data.items)
    },
    post_new_contact(contact: NewContactValuesType) {
        return instance.post(`contacts/new`, contact).then((response: AxiosResponse<NewContactValuesType>) => response.data)
    },
    get_contact_by_id(id: number){
      return instance.get(`contacts/${id}`).then((response: AxiosResponse<ContactType>) => response.data)
    },
    update_contact(contact: ContactType){
        return instance.put(`contacts/${contact.id}`, contact).then((response: AxiosResponse<ContactType>) => response.data)
    },
    get_roles(){
        return instance.get(`roles`).then((response: AxiosResponse<{items: string[]}>) => response.data.items)
    }
}