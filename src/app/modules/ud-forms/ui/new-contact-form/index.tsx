import React, {useEffect} from 'react';
import './styles.scss';
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import {ContactType} from "../../../contacts/store/contacts-list/actions";
import {Roles} from "../../../contacts/ui/components/new-contact-card";
import {maxLengthCreator, required} from "../../../validation/common-validator";
import UDInput from "../../../ud-ui/form-input/input";


export type NewContactValuesType = {
    name: string,
    email: string,
    company: string,
    role: string
};

const maxLength10 = maxLengthCreator(10);
const maxLength20 = maxLengthCreator(20);
const maxLength40 = maxLengthCreator(40);

export const NewContactForm: React.FC<InjectedFormProps<ContactType, Roles> & Roles> = ({handleSubmit, error, roles}) => {
    console.log(roles)
    return <form
        onSubmit={handleSubmit}
    >
        <div className="row align-items-center justify-content-between p-3">
            <div className="col container m-3">
                <Field
                    placeholder={"Name"}
                    component={UDInput}
                    validate={[required, maxLength10]}
                    name={"name"}
                    className="col-12 regular-text fs15 mt-5"
                />
                <Field
                    placeholder={"Email"}
                    component={UDInput}
                    validate={[required, maxLength40]}
                    name={"email"}
                    className="col-12 regular-text fs15 mt-5"
                />
                <Field
                    placeholder={"Company"}
                    component={UDInput}
                    validate={[required, maxLength20]}
                    name={"company"}
                    className="col-12 regular-text fs15 mt-5"
                />
                <Field
                    placeholder={"Role"}
                    component={"select"}
                    name={"role"}
                    className="col-12 regular-text fs15 input-text-new mt-5 py-2 px-0"
                >
                    {roles.map(role =>
                        <option value={role}>{role}</option>
                    )}
                </Field>
                <div className="row align-items-center p-0">
                    <a href="/contacts"><p className="semibold-text fs15 blue row justify-content-center m-3">Cancel</p>
                    </a>
                    <button type="submit" className="outlined-button semibold-text fs15 m-3">Create</button>
                </div>
            </div>
            <div className="col m-3">
                <label className="select-avatar-input-label" htmlFor="labelHTMLFor">Select avatar</label>
                <input type="file" id="labelHTMLFor" className="select-avatar-input"/>
            </div>
        </div>
    </form>;
};

const NewContactReduxForm = reduxForm<ContactType, Roles>({form: 'new_contact'})(NewContactForm)

export default NewContactReduxForm;
