import React from "react";
import {Field, InjectedFormProps, reduxForm} from "redux-form";

const SignUp_form: React.FC<InjectedFormProps<SignUpValuesType>> = ({handleSubmit, error}) => {
    return <form className="d-flex flex-column"
                 onSubmit={handleSubmit}>
        <div className="d-flex flex-column my-4">
            <label>
                <p className="regular-text disabled-text fs15 m-0">
                    Email
                </p>
            </label>
            <Field component={"input"} name={"email"} className="input-text-new my-2"/>
            <label>
                <p className="regular-text disabled-text fs15 m-0">
                    Password
                </p>
            </label>
            <Field component={"input"} name={"password"} className="input-text-new my-2"/>
        </div>
        <div className="row align-items-center justify-content-center m-3">
            {/*<Field name={"agreed"} component={(props: { value: boolean; onChange: (arg0: any) => any; }) =>*/}
            {/*    <UDCheckbox*/}
            {/*        flag={props.value}*/}
            {/*    />*/}
            {/*}/>*/}
            <Field type="checkbox" name={"agreed"} component={"input"}/>
            <p className="regular-text fs12 m-2">I agree with you completely</p>
        </div>
        <div className="row align-items-center justify-content-center">
            <button type="submit">Sign in</button>
        </div>
    </form>
};

export type SignUpValuesType = {
    email: string
    password: string
    agreed: boolean
}

const SignUpReduxForm = reduxForm<SignUpValuesType>({form: 'sign_up'})(SignUp_form)

export default SignUpReduxForm;