import React from "react";
import {Field, InjectedFormProps, reduxForm} from "redux-form";
import UDInput from "../../../ud-ui/form-input/input";
import {required} from "../../../validation/common-validator";

const SignIn_form: React.FC<InjectedFormProps<SignInValuesType>> = ({handleSubmit, error}) => {
    debugger
    return <form className="d-flex flex-column"
                 onSubmit={handleSubmit}>
        <div className="d-flex flex-column my-4">
            {{error} && <p>{error}</p>}
            <label>
                <p className="regular-text disabled-text fs15 m-0">
                    Email
                </p>
            </label>
            <Field component={UDInput}
                   validate={[required]}
                   name={"email"}
                   className="input-text-new my-2"/>

            <label>
                <p className="regular-text disabled-text fs15 m-0">
                    Password
                </p>
            </label>
            <Field component={UDInput}
                   validate={[required]}
                   type="password"
                   name={"password"}
                   className="input-text-new my-2"/>
        </div>
        <div className="row align-items-center justify-content-center">
            <button type="submit">Sign in</button>
        </div>
    </form>
};

export type SignInValuesType = {
    email: string
    password: string
}

const SignInReduxForm = reduxForm<SignInValuesType>({form: 'sign_in'})(SignIn_form)

export default SignInReduxForm;