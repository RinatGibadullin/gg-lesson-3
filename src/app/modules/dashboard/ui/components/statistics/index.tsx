import React from 'react';
import './styles.scss';
import UDCircle from "../../../../ud-ui/task-circle/component";

type props = {
    activeTasks?: Number,
    completedTasks?: Number,
    endedTasks?: Number
};

const TasksStat = (props: props) => {
    return <div className="statistics-card">
        <div className="container">
            <div className="row align-items-center justify-content-between px-4">
                <p className="medium-text fs15">Tasks</p>
                <p className="regular-text disabled-text fs12">Show: This month</p>
            </div>
        </div>
        <hr/>
        <div className="container-fluid">
            <div className="row align-items-center justify-content-between px-4">
                <div className="col statistics-circle"/>
                <div className="col pl-4">
                    <div className="row align-items-center">
                        <UDCircle color="yellow"/>
                        <p className="regular-text fs12">Active</p>
                    </div>
                    <div className="row align-items-center">
                        <UDCircle color="green"/>
                        <p className="regular-text fs12">Completed</p>
                    </div>
                    <div className="row align-items-center">
                        <UDCircle color="red"/>
                        <p className="regular-text fs12">Ended</p>
                    </div>
                </div>
            </div>
        </div>
    </div>;
};

export default TasksStat;
