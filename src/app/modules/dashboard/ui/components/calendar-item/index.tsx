import React from 'react';
import './styles.scss';


type calendarItemProps = {
    day: String,
    date: String
};

const CalendarItem = (props: calendarItemProps) => {
    return <span className="calendar-item">
        <p className="regular-text disabled-text fs13">{props.day}</p>
        <p className="regular-text fs13">{props.date}</p>
    </span>
};

export default CalendarItem;