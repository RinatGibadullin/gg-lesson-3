import React from 'react';
import TasksItem from "../tasks-item";

type tasksListProps = {};

const TasksList = (props: tasksListProps) => {
    return (
        <div>
            <TasksItem
                title="Send benefit review by Sunday"
                type="Reminder"
                dueDate="December 23, 2018"
                assignedBy="George Fields"
                status="Completed"
            />
            <TasksItem
                title="Invite to office meet-up"
                type="Call"
                dueDate="December 23, 2018"
                assignedBy="Rebecca Moore"
                status="Ended"
            />
            <TasksItem
                title="Office meet-up"
                type="Event"
                dueDate="December 23, 2018"
                assignedBy="Lindsey Stroud"
                status="Active"
            />
        </div>
    );
};

export default TasksList;