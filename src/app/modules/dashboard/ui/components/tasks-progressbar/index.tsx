import React from 'react';
import './styles.scss'
import UDProgressBar from "../../../../ud-ui/progress-bar/progress_bar";
import UDDropDownMenu, {UDDropDownMenuItem} from "../../../../ud-ui/select/select";

type progressbarProps = {
    completed: number,
    total: number
};

const TasksProgressbar = (props: progressbarProps) => {
    return <div className="container">
        <div className="row align-items-center justify-content-between px-4 py-1">
            <p className="semibold-text fs13">{props.completed} task completed out of {props.total}</p>
            <div className="row">
                <p className="regular-text disabled-text fs12">Show: </p>
            </div>
        </div>
        <UDProgressBar value={props.completed / props.total * 100}/>
    </div>
};

export default TasksProgressbar;