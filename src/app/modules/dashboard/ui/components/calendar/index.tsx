import React from 'react';
import CalendarItem from "../calendar-item";

type calendarProps = {};

const Calendar = (props: calendarProps) => {
    return <div className="container">
        <div className="row align-items-center justify-content-between px-4">
            <p className="semibold-text fs15">23 December, Sunday</p>
        </div>
        <div className="row align-items-center justify-content-between px-4">
            <CalendarItem day="Sun" date="23"/>
            <CalendarItem day="Mon" date="24"/>
            <CalendarItem day="Tue" date="25"/>
            <CalendarItem day="Wed" date="26"/>
            <CalendarItem day="Thu" date="27"/>
            <CalendarItem day="Fri" date="28"/>
            <CalendarItem day="Sat" date="29"/>
        </div>

    </div>
};

export default Calendar;