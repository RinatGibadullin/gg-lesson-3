import React from 'react';
import './styles.scss';
import TasksList from "../tasks-list";
import TasksProgressbar from "../tasks-progressbar";
import Calendar from "../calendar";

type TasksCardProps = {
    children?: any
};

const TasksCard = (props: TasksCardProps) => {
    return <div className="tasks-card">
        <TasksProgressbar completed={8} total={10}/>
        <Calendar/>
        <hr/>
        <TasksList/>
        <a href="#"><p className="semibold-text fs15 blue row justify-content-center">Show more</p></a>
    </div>;
};

export default TasksCard;