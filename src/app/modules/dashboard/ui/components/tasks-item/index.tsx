import React from 'react';
import './styles.scss'
import UDBadge, {badgeProps} from "../../../../ud-ui/badge/badge";

type itemProps = {
    title?: String,
    dueDate?: String,
    type?: String,
    assignedBy?: String,
    status: "Completed" | "Active" | "Ended"
};

const TasksItem = (props: itemProps) => {
    return <div className="tasks-item-card">
        <div className="container">
            <div className="row align-items-center justify-content-between px-3">
                <p className="medium-text fs15">{props.title}</p>
                <p className="regular-text disabled-text fs12">{props.type}</p>
            </div>
            <div className="row align-items-center px-3">
                <p className="regular-text disabled-text fs12 m-0">DueDate: </p>
                <p className="regular-text fs12 m-0">{props.dueDate}</p>
            </div>
            <div className="row align-items-center justify-content-between px-3">
                <div className="row align-items-center justify-content-center px-3">
                    <div className="assigned-avatar"/>
                    <p className="regular-text disabled-text fs12 px-2">{props.assignedBy}</p>
                </div>
                <UDBadge label={props.status}/>
            </div>
        </div>
    </div>;
};

export default TasksItem;