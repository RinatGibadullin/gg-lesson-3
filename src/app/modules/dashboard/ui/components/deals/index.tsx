import React from 'react';
import './styles.scss'
import UDCircle from "../../../../ud-ui/task-circle/component";

type props = {
    children?: any,
    plot?: any
};

const Deals = (props: props) => {
    return <div className="deals-card">
        <div className="container">
            <div className="row align-items-center justify-content-between px-4">
                <p className="medium-text fs15">Deals</p>
                <p className="regular-text disabled-text fs12">Show: This month</p>
            </div>
        </div>
        <hr/>
        <div className="container">
            <div className="row align-items-center px-4">
                <UDCircle color="blue"/>
                <p className="regular-text fs12">Closed deals</p>
            </div>
            <div className="deals-graphic-image"/>
        </div>

    </div>;
};

export default Deals;
