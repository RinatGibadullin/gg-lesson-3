import React from 'react';
import Col from "../../../../grid/ui/Col";
import TasksCard from "../../components/tasks";
import Deals from "../../components/deals";
import TasksStat from "../../components/statistics";

type dashboardProps = {};

const Dashboard = (props: dashboardProps) => {
    return <div className="container-fluid">
        <div className="row justify-content-center">
            <div className="col col-lg-8">
                <TasksCard/>
            </div>
            <div className="col col-lg-4">
                <Deals/>
                <TasksStat/>
            </div>
        </div>
    </div>;
};

export default Dashboard;
