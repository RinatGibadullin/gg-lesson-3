import React from "react";

const justify = (props: { justifyContent: string | undefined }) => `justify-content-${props.justifyContent}`;

const align = (props: { alignItems: string | undefined }) => `align-items-${props.alignItems}`;

type RowProps = {
    justifyContent?: string,
    alignItems?: string
}

const Row: React.FC<RowProps> = ({justifyContent, alignItems, children}) => (
    <div className={`row ${justify({justifyContent})} ${align({alignItems})}`}>
        {children}
    </div>
);

export default Row;