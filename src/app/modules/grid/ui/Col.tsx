import React from 'react';

const colClass = (props: { col: string | undefined }) => `${props.col}`;

const widthClass = (props: { width: string | undefined }) => `${props.width}`;

type ColProps = {
    col?: string,
    width?: string
}

const Col: React.FC<ColProps> = ({col, width, children}) => (
    <div className={`col col-${widthClass({width})}-${colClass({col})}`}>
        {children}
    </div>
);

export default Col;