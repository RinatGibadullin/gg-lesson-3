import React from 'react';

const fluidClass = (props: { fluid: boolean | undefined }) =>
    props.fluid ? `-fluid` : "";

type ContainerProps = {
    fluid?: boolean
}

const Container: React.FC<ContainerProps> = ({fluid, children}) => (
    <div className={`container${fluidClass({fluid})}`}>
        {children}
    </div>
);

export default Container;
