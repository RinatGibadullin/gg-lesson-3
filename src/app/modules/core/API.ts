const axios = require('axios').default;

const instance = axios.create({
    baseURL: "https://private-90f92-saasapi.apiary-mock.com/"
})

export default instance;