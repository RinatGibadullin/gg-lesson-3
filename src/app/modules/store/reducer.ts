import {combineReducers} from "redux";
import {layoutRootReducer} from "../layout/store/reducer";

import {reducer as formReducer} from 'redux-form'
import {contactsRootReducer} from "../contacts/store/reducer";
import {sessionRootReducer} from "../session/store/reducer";

export const rootReducer = combineReducers({
    layout: layoutRootReducer,
    form: formReducer,
    contacts: contactsRootReducer,
    session: sessionRootReducer
});

export type RootStateType = ReturnType<typeof rootReducer>
