import React from "react";
import {StyledBlueCircle, StyledCircle, StyledGreenCircle, StyledRedCircle, StyledYellowCircle} from "./styles";

type CircleProps = {
    color: 'red' | 'yellow' | 'green' | 'blue'
};

const UDCircle = (props: CircleProps) => {
    if (props.color === 'red') return <StyledRedCircle/>;
    if (props.color === 'yellow') return <StyledYellowCircle/>;
    if (props.color === 'green') return <StyledGreenCircle/>;
    if (props.color === 'blue') return <StyledBlueCircle/>;
    else return <StyledCircle/>
};

export default UDCircle;