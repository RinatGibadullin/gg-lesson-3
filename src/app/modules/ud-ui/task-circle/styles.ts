import styled from 'styled-components';

export const StyledCircle = styled.div`
    margin: 10px;
    width: 8px;
    height: 8px;
    background-color: transparent;
    border-radius: 50px;
    border: 2px solid black;
`;

export const StyledRedCircle = styled(StyledCircle)`
    border: 2px solid #F7685B;
`;

export const StyledYellowCircle = styled(StyledCircle)`
    border: 2px solid #FFB946;
`;
export const StyledGreenCircle = styled(StyledCircle)`
    border: 2px solid #2ED47A;
`;

export const StyledBlueCircle = styled(StyledCircle)`
    border: 2px solid #109CF1;
`;