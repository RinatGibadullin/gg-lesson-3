import React from "react";
import {StyledInput} from "./styles";

const UDInput = ({input, meta, ...props}: any) => {
    const hasError = meta.touched && meta.error
    return (
        <>
            <StyledInput hasError={hasError} {...input} {...props}/>
            {hasError && <p className="regular-text error-text fs13 m-0 p-0">{meta.error}</p>}
        </>
    )
};

export default UDInput;