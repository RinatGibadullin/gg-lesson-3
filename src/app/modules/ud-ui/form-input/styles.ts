import styled from 'styled-components';

export const StyledInput = styled.input`
    ${(props: { hasError: string }) =>
        props.hasError
            ? "border: 2px solid #D91513;" +
            "border-radius: 5px;"
            : "border: none;" +
            "border-bottom: 2px solid #D3D8DD;"
    }
    background-color: transparent;
    width: inherit;
    padding: 10px 5px 10px 5px;
    &:focus {
    outline: none;
  }
`;