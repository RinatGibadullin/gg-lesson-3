import React from "react";
import {StyledActiveBadge, StyledCompletedBadge, StyledBadge, StyledEndedBadge} from './styles'

// const backgroundColorClass = (props: { type: "Completed" | "Active" | "Ended" | undefined }) => {
//     if (props.type === "Ended") {
//         return "#F7685B"
//     }
//     if (props.type === "Active") {
//         return "#FFB946"
//     }
//     if (props.type === "Completed") {
//         return "#2ED47A "
//     } else {
//         return ""
//     }
// };

export type badgeProps = {
    label: String,
    type?: "success" | "warning" | "danger"
};

const UDBadge = (props: badgeProps) => {
    if (props.label === "Ended") {
        return <StyledEndedBadge>
            {props.label}
        </StyledEndedBadge>;
    }
    if (props.label === "Active") {
        return <StyledActiveBadge>
            {props.label}
        </StyledActiveBadge>;
    }
    if (props.label === "Completed") {
        return <StyledCompletedBadge>
            {props.label}
        </StyledCompletedBadge>;
    } else {
        return <StyledBadge>
            {props.label}
        </StyledBadge>;
    }
};

export default UDBadge;