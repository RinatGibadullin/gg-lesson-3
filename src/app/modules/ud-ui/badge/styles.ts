import styled from 'styled-components';

export const StyledBadge = styled.div`
    width: 84px;
    height: 22px;
    border-radius: 4px;
    background-color: black;
    padding: 5px;
    text-align: center;
    font-size: 12px;
    color: white;
    margin: 15px 0 15px 15px;
`;

export const StyledEndedBadge = styled(StyledBadge)`
    background-color: #F7685B;
`;

export const StyledCompletedBadge = styled(StyledBadge)`
    background-color: #2ED47A;
`;

export const StyledActiveBadge = styled(StyledBadge)`
    background-color: #FFB946;
`;