import React from "react";
import {StyledButton, StyledLgButton, StyledMdButton, StyledSmButton} from './styles'

type buttonProps = {
    label: string,
    href: string,
    type: 'sm' | 'md' | 'lg'
};

const UDButton = (props: buttonProps) => {
    if (props.type === 'sm'){
        return <StyledSmButton>
            {props.label}
        </StyledSmButton>;
    }
    if (props.type === 'md'){
        return <StyledMdButton>
            {props.label}
        </StyledMdButton>;
    }
    if (props.type === 'lg'){
        return <StyledLgButton href={props.href}>
            {props.label}
        </StyledLgButton>
    }
    else return <StyledButton>
        {props.label}
    </StyledButton>;
};

export default UDButton;