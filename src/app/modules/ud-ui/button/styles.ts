import styled from 'styled-components';

export const StyledButton = styled.a`
    border-radius: 4px;
    border: none;
    cursor: pointer;
    box-shadow: 0px 4px 10px rgba(16, 156, 241, 0.24);
    background-color: #109CF1;
    text-align: center;
    vertical-align: middle;
    color: white;
    &:hover {
        background-color: #34AFF9;
    };
    &:focus {
        background-color: #098EDF;
    }
`;

export const StyledLgButton = styled(StyledButton)`
    font-size: 14px;
    padding: 10px 50px 10px 50px;
`;

export const StyledMdButton = styled(StyledButton)`
    width: 100px;
    height: 25px;
`;

export const StyledSmButton = styled(StyledButton)`
    width: 40px;
    height: 10px;
`;