import React from "react";

export type UDDropDownMenuItem = () => {
    label: string,
    key: string,
    icon?: string
}

type selectProps = {
    options: UDDropDownMenuItem[],
    classNames?: string,
    onSelect?: (item: UDDropDownMenuItem) => void
};

const UDDropDownMenu = (props: selectProps) => {
    return <select>
        {props.options.map(item =>
            <option>{item}</option>
        )}
    </select>;
};

UDDropDownMenu.defaultProps = {
    onSelect: () => {
    }
}

export default UDDropDownMenu;