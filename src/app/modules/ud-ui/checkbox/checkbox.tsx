import React from "react";

export type checkboxProps = {
    flag: boolean,
};

const UDCheckbox = (props: checkboxProps) => {
    if (props.flag) return <input type="checkbox" checked/>;
    else {
        return <input type="checkbox"/>;
    }
};

export default UDCheckbox;