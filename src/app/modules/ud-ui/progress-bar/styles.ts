import styled from 'styled-components';

export const StyledProgressBar = styled.div`
  height: 5px;  /* Can be anything */
  position: relative;
  background: #c9c9c9;
  -moz-border-radius: 25px;
  -webkit-border-radius: 25px;
  border-radius: 25px;
`;

export const StyledProgressBarValue = styled.span<{progress: string}>`
  display: block;
  height: 100%;
  border-top-right-radius: 8px;
  border-bottom-right-radius: 8px;
  border-top-left-radius: 20px;
  border-bottom-left-radius: 20px;
  background-color: #2ED47A;
  position: relative;
  overflow: hidden;
  width: ${props => props.progress};
`;