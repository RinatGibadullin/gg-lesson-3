import React from "react";
import {StyledProgressBar, StyledProgressBarValue} from './styles'

type ProgressBarProps = {
    value: Number
};

const UDProgressBar = (props: ProgressBarProps) => {
    return <StyledProgressBar>
        <StyledProgressBarValue progress={`${props.value}%`}/>
    </StyledProgressBar>
};

export default UDProgressBar;