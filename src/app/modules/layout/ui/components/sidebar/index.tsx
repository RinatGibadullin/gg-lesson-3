import React from "react";
import './styles.scss';
import UDSidebarUserInfo from "../sidebar-user-info";
import {connect, ConnectedProps} from 'react-redux'
import {TOGGLE_SIDEBAR} from "../../../store/sidebar/types";
import {RootStateType} from "../../../../store/reducer";
import UDSidebarItem from "../sidebar-item";

const mapState = (state: RootStateType) => ({
    sidebar_is_open: state.layout.sidebar.is_open
});

const mapDispatch = {
    toggleSidebar: () => ({type: TOGGLE_SIDEBAR}),
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

type sideBarProps = PropsFromRedux & {}

const UDSideBar = (props: sideBarProps) => {
    return <div className={"sidebar-card m-0" + (props.sidebar_is_open ? "" : " closed-sidebar-card p-0")}>
        <p className="semibold-text fs18 pl-4 pr-2 pt-0 pb-0 blue">
            {props.sidebar_is_open ? ("SaaS Kit") : ("SaaS")}
        </p>
        <hr/>
        <div className={props.sidebar_is_open ? "mr-4" : ""}>
            <UDSidebarUserInfo name="Sierra Ferguson" email="s.ferguson@gmail.com"/>
        </div>
        <div className="container my-4">
            <UDSidebarItem href="/dashboard" iconClass="dashboard-icon" title="Dashboard"/>
            <UDSidebarItem href="/tasks" iconClass="tasks-icon" title="Tasks"/>
            <UDSidebarItem href="/email" iconClass="email-icon" title="Email"/>
            <UDSidebarItem href="/contacts" iconClass="contacts-icon" title="Contacts"/>
            <UDSidebarItem href="/chat" iconClass="chat-icon" title="Chat"/>
            <UDSidebarItem href="/deals" iconClass="deals-icon" title="Deals"/>
        </div>
        <hr/>
        <div className="container">
            <UDSidebarItem href="/settings" iconClass="settings-icon" title="Settings"/>
            <a href="#" className="row d-flex flex-nowrap align-items-center px-4 pt-5"
               onClick={
                   props.toggleSidebar
               }>
                <svg
                    className={`${props.sidebar_is_open ? "sidebar-toggle-icon " : "sidebar-toggle-active-icon"} my-3`}/>
                <p className="medium-text disabled-text fs15 mx-3 my-0 sidebar-item-value">toggle sidebar</p>
            </a>
        </div>
    </div>
};

export default connector(UDSideBar);