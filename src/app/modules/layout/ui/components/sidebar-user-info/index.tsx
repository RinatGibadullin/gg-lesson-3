import React from "react";
import './styles.scss';

type userInfoProps = {
    name: String,
    email: String,
};

const UDSidebarUserInfo = (props: userInfoProps) => {
    return <div className="container">
        <div className="row d-flex flex-nowrap align-items-center justify-content-left m-0 p-0">
            <div className="profile-avatar my-3"/>
            <div className="ml-3">
                <p className="medium-text fs15 mb-0 sidebar-item-value">{props.name}</p>
                <p className="medium-text disabled-text fs12 mt-0 sidebar-item-value">{props.email}</p>
            </div>
        </div>
    </div>
};

export default UDSidebarUserInfo;