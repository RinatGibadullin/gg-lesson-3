import React from "react";
import './styles.scss';
import {Field, InjectedFormProps, reduxForm} from "redux-form";

const UDSearchBarForm: React.FC<InjectedFormProps<SearchValuesType>> = ({handleSubmit, error}) => {
    return <form onSubmit={handleSubmit}>
        <Field
            placeholder={"Global search"}
            component={"input"}
            name={"value"}
            className="medium-text fs14 m-3 search-bar-input"
        />
    </form>
};

export type SearchValuesType = {
    value: string
}

const UDSearchBarReduxForm = reduxForm<SearchValuesType>({form: 'search-global'})(UDSearchBarForm)

export default UDSearchBarReduxForm;