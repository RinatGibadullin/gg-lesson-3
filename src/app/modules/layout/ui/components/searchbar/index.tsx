import React from "react";
import './styles.scss';
import UDSearchBarReduxForm, {SearchValuesType} from "../searchbar-input";
import {withRouter, RouteComponentProps } from "react-router-dom";
import {RootStateType} from "../../../../store/reducer";
import {connect, ConnectedProps} from "react-redux";
import {requestGlobalSearch} from "../../../store/searchbar/reducer";

const mapState = (state: RootStateType) => ({
});

const mapDispatch = {
    requestGlobalSearch
};

const connector = connect(mapState, mapDispatch);

type PropsFromRedux = ConnectedProps<typeof connector>

type SearchBarProps = PropsFromRedux & RouteComponentProps

const UDSearchBar: React.FC<SearchBarProps> = (props) => {

    const onSubmit = (formData: SearchValuesType) => {
        props.history.push("/search/global?value=" + formData.value)
    }

    return <div className="search-bar">
        <div className="container-fluid">
            <div className="row align-items-center justify-content-between px-5">
                <div className="d-flex flex-nowrap align-items-center">
                    <div className="search-icon"/>
                    <UDSearchBarReduxForm onSubmit={onSubmit}/>
                </div>
                <div className="notification-icon"/>
            </div>
        </div>
    </div>;
};

export default connector(withRouter(UDSearchBar));
