import React from "react";
import './styles.scss';
import {NavLink} from "react-router-dom";

type sidebarItemProps = {
    iconClass: string,
    title: string,
    href: string
};

const UDSidebarItem = (props: sidebarItemProps) => {
    const path = window.location.pathname
    console.log(path)
    return <NavLink to={props.href} className="row d-flex flex-nowrap align-items-center px-4 py-3"
                    activeClassName="active-icon">
        <div className={props.iconClass}/>
        <p className="medium-text fs15 mx-3 my-0 sidebar-item-value">{props.title}</p>
    </NavLink>;
};

export default UDSidebarItem;