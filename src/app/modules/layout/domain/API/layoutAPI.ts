import {AxiosResponse} from "axios";
import instance from "../../../core/API";

export const layoutAPI = {
    global_search_by_value(value: string){
        return instance.get(`search/global?value=${value}`).then((response: AxiosResponse<{items: string[]}>) => response.data.items)
    },
}