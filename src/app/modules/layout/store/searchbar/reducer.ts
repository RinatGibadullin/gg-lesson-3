import {SET_GLOBAL_SEARCH_VALUE, setGlobalSearchValue, SetGlobalSearchValueActionType} from "./actions";
import {Dispatch} from "redux";
import {RootStateType} from "../../../store/reducer";
import {contactsAPI} from "../../../contacts/domain/API/contactsAPI";
import {layoutAPI} from "../../domain/API/layoutAPI";

const searchBarInitialState = {
    global_search_value: "" as string
};

export type SearchBarInitialStateType = typeof searchBarInitialState

export function searchbarReducer(
    state = searchBarInitialState,
    action: SetGlobalSearchValueActionType
): SearchBarInitialStateType {
    switch (action.type) {
        case SET_GLOBAL_SEARCH_VALUE: {
            return {
                ...state,
                 global_search_value: action.value
            }
        }
        default:
            return state
    }
}

export const requestGlobalSearch = (value: string) => {
    return async (dispatch: Dispatch<SetGlobalSearchValueActionType>, getState: RootStateType) => {
        let data = await layoutAPI.global_search_by_value(value)
        dispatch(setGlobalSearchValue(data))
    }
}