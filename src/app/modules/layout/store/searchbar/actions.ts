export const SET_GLOBAL_SEARCH_VALUE = 'SET_GLOBAL_SEARCH_VALUE';

export type SetGlobalSearchValueActionType = {
    type: typeof SET_GLOBAL_SEARCH_VALUE
    value: string
}

export const setGlobalSearchValue = (value: string): SetGlobalSearchValueActionType => ({type: SET_GLOBAL_SEARCH_VALUE, value});
