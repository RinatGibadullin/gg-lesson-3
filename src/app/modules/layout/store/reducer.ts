import {combineReducers} from "redux";
import {sidebarReducer} from "./sidebar/reducer";
import {searchbarReducer} from "./searchbar/reducer";

export const layoutRootReducer = combineReducers({
    sidebar: sidebarReducer,
    searchbarReducer: searchbarReducer
});

export type LayoutRootStateType = ReturnType<typeof layoutRootReducer>
