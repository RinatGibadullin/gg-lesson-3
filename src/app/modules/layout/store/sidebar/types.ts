export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR';

export type SidebarStateType = {
    is_open: boolean
}

type ToggleSidebarActionType = {
    type: typeof TOGGLE_SIDEBAR
    payload: SidebarStateType
}

export type SidebarActionTypes = ToggleSidebarActionType
