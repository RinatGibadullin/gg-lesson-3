import {SidebarActionTypes, SidebarStateType, TOGGLE_SIDEBAR} from './types'

const initialState: SidebarStateType = {
    is_open: localStorage.getItem("sidebar_is_open") === "true"
};

export function sidebarReducer(
    state = initialState,
    action: SidebarActionTypes
): SidebarStateType {
    switch (action.type) {
        case TOGGLE_SIDEBAR: {
            localStorage.setItem("sidebar_is_open", String(!state.is_open))
            return {
                ...state,
                is_open: !state.is_open
            }
        }
        default:
            return state
    }
}
