import {SidebarActionTypes, SidebarStateType, TOGGLE_SIDEBAR} from './types'

export function toggle_sidebar(toggleSidebar: SidebarStateType): SidebarActionTypes {
    return {
        type: TOGGLE_SIDEBAR,
        payload: toggleSidebar
    }
}
