import React from 'react';
import './styles/libs/bootstrap.scss'
import './styles/styles.scss'
import {AppRouter} from "./AppRouter";

const App = () =>
    <div className="container-fluid inherit-height m-0 p-0">
        <AppRouter/>
    </div>

export default App;
