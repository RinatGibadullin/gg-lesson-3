import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    useParams
} from "react-router-dom";
import Dashboard from "./app/modules/dashboard/ui/pages/index";
import Contacts from "./app/modules/contacts/ui/pages/index";
import NewContact from "./app/modules/contacts/ui/pages/new/new";
import SignIn from "./app/modules/session/ui/pages/sign-in";
import SignUp from "./app/modules/session/ui/pages/sign-up";
import EditContact from "./app/modules/contacts/ui/pages/edit/edit";
import UDSideBar from "./app/modules/layout/ui/components/sidebar";
import UDSearchBar from "./app/modules/layout/ui/components/searchbar";


const MainPagesRouter = () =>
    <>
        <Route exact path="/dashboard">
            <Dashboard/>
        </Route>
        <Route exact path="/contacts">
            <Contacts/>
        </Route>
        <Route exact path="/contacts/new">
            <NewContact/>
        </Route>
        <Route exact path="/contacts/edit/:id">
            <EditContact/>
        </Route>
    </>


export const AppRouter = () => {
    return (
        <Router>
            <Switch>
                <Route exact path="/users/sign-in">
                    <SignIn/>
                </Route>
                <Route exact path="/users/sign-up">
                    <SignUp/>
                </Route>
                <div className="d-flex flex-nowrap inherit-height">
                    <UDSideBar/>
                    <div className="container-fluid m-0 p-0">
                        <UDSearchBar/>
                        <MainPagesRouter/>
                    </div>
                </div>
            </Switch>
        </Router>
    )
}